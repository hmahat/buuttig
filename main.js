let a = [1, 2, 3, 4];

function multiply(a, m) {
  return a.map((i) => i * m);
}

console.log(multiply(a, 3));
console.log(multiply(a, 5));
